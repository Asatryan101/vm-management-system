package com.system.config.mapping;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperFactory {

    @Bean
    public UserMapper userMapper() {
        return UserMapper.INSTANCE;
    }

    @Bean
    public VirtualMachineMapper virtualMachineMapper() {
        return VirtualMachineMapper.INSTANCE;
    }
}
