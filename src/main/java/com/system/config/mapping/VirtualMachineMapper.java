package com.system.config.mapping;

import com.system.data.jpa.entity.VirtualMachineEntity;
import com.system.service.virtualMachine.DTO.*;
import com.system.web.model.virtualMachine.*;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VirtualMachineMapper {

    VirtualMachineMapper INSTANCE = Mappers.getMapper(VirtualMachineMapper.class);

    VirtualMachineDTO toDTO(VirtualMachineEntity virtualMachineEntity);

    VirtualMachineAssignmentDTO toAssignmentDTO(VirtualMachineEntity virtualMachineEntity);

    VirtualMachineEditDTO toDTO(VirtualMachineEditModel vmEditModel);

    VirtualMachineModel toModel(VirtualMachineDTO virtualMachineDTO);

    VirtualMachineAssignmentModel toModel(VirtualMachineAssignmentDTO virtualMachineAssignmentDTO);
}
