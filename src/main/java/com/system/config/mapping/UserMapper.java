package com.system.config.mapping;

import com.system.service.user.DTO.UserCreateDTO;
import com.system.service.user.DTO.UserRegistrationDTO;
import com.system.web.model.auth.UserRegisterModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "encodedPassword", ignore = true)
    @Mapping(target = "roles", ignore = true)
    UserCreateDTO mapRegistrationDTOToCreateDTO(UserRegistrationDTO registrationDto);

    UserRegistrationDTO mapRegisterModelToDto(UserRegisterModel userRegisterModel);


}
