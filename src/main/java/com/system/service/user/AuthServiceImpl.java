package com.system.service.user;


import com.system.exception.UserAlreadyExistsException;
import com.system.service.user.DTO.RoleDTO;
import com.system.service.user.DTO.UserCreateDTO;
import com.system.service.user.DTO.UserRegistrationDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserService userService;
    private final RoleService roleService;

    public void register(UserRegistrationDTO user) {
        checkIfUserAlreadyExists(user);
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        UserCreateDTO userCreateDTO = new UserCreateDTO();
        userCreateDTO.setFirstName(user.getFirstName());
        userCreateDTO.setUsername(user.getUsername());
        userCreateDTO.setEncodedPassword(encodedPassword);
        userCreateDTO.setLastName(user.getLastName());
        userCreateDTO.setPhoneNumber(user.getPhoneNumber());
        userCreateDTO.setRoles(Collections.singletonList(getUserRole()));
        userService.create(userCreateDTO);
    }


    private RoleDTO getUserRole() {
        return roleService.getByName("ROLE_USER");
    }


    private void checkIfUserAlreadyExists(UserRegistrationDTO user) {
        if (userService.isUserExistWithUsername(user.getPhoneNumber())) {
            throw new UserAlreadyExistsException(user.getPhoneNumber());
        }
    }


}

