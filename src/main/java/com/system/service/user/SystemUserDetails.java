package com.system.service.user;

import org.springframework.security.core.userdetails.UserDetails;

public interface SystemUserDetails extends UserDetails {

    Long getId();

}
