package com.system.service.user;

import com.system.data.jpa.entity.RoleEntity;
import com.system.data.jpa.repository.RoleRepository;
import com.system.exception.RoleNotFoundException;
import com.system.service.user.DTO.RoleDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public RoleDTO getByName(String name) {
        RoleEntity role = roleRepository.findByName(name).orElseThrow(() -> new RoleNotFoundException(name));
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(role.getId());
        roleDTO.setName(role.getName());
        return roleDTO;
    }
}
