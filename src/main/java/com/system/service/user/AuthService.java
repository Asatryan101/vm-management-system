package com.system.service.user;


import com.system.service.user.DTO.UserRegistrationDTO;

public interface AuthService {

  void register(UserRegistrationDTO registrationDto);

}
