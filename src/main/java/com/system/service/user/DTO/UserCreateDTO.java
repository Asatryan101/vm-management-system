package com.system.service.user.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserCreateDTO {

    private String username;

    private String phoneNumber;

    private String firstName;

    private String lastName;

    private String encodedPassword;

    private List<RoleDTO> roles;

}
