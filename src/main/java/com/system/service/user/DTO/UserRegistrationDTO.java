package com.system.service.user.DTO;

import lombok.Getter;
import lombok.Setter;
import lombok.Value;

@Getter
@Setter
public class UserRegistrationDTO {

    private String phoneNumber;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

}
