package com.system.service.user.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserDTO {

    private Long id;

    private String username;

    private String key;

    private List<RoleDTO> roles;

}
