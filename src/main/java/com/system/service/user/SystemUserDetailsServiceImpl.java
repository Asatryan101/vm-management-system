package com.system.service.user;

import com.system.data.jpa.entity.RoleEntity;
import com.system.data.jpa.entity.UserEntity;
import com.system.data.jpa.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


@Service(value = "userDetailsService")
@RequiredArgsConstructor
public class SystemUserDetailsServiceImpl implements SystemUserDetailsService {


    private final UserRepository userRepository;


    @Override
    @Transactional(readOnly = true)
    public SystemUserDetails loadUserByUsername(String username) {
        UserEntity userEntity = userRepository.findByUsernameAndDeletedFalse(username).orElseThrow(() -> new UsernameNotFoundException(
                String.format("User not found with this username %s", username)));

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (RoleEntity role : userEntity.getRoles()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return new SystemUser(userEntity.getId(), userEntity.getUsername(), userEntity.getEncodedPassword(), grantedAuthorities);

    }

}
