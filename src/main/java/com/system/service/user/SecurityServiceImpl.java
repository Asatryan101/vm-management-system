package com.system.service.user;

import com.nimbusds.jwt.SignedJWT;
import com.system.config.security.jwt.JwtTokenProvider;
import com.system.data.jpa.entity.RefreshTokenEntity;
import com.system.data.jpa.entity.RoleEntity;
import com.system.data.jpa.entity.UserEntity;
import com.system.data.jpa.repository.RefreshTokenRepository;
import com.system.data.jpa.repository.UserRepository;
import com.system.exception.RefreshTokenNotFoundException;
import com.system.exception.UserNotFoundException;
import com.system.service.RandomGenerator;
import com.system.service.user.DTO.UserRegistrationDTO;
import com.system.web.model.auth.AuthorizationResponseModel;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SecurityServiceImpl implements SecurityService {

    private Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);
    private static final int REFRESH_TOKEN_LENGTH = 255;
    private final AuthenticationManager authenticationManager;
    private final SystemUserDetailsService systemUserDetailsService;
    private final UserRepository userRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final RefreshTokenRepository refreshTokenRepository;
    private final RandomGenerator randomGenerator;
    private final AuthService authService;

    @Override
    @Transactional
    public AuthorizationResponseModel authenticate(String username, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password, new ArrayList<>()));
        SystemUserDetails userDetails = systemUserDetailsService.loadUserByUsername(username);
        String token = jwtTokenProvider.createToken(username, userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(
                Collectors.toSet()));
        logger.info("user logged in with username {} and password {}", username, password);
        return createAuthorizationResponse(userDetails, token);
    }


    private AuthorizationResponseModel createAuthorizationResponse(SystemUserDetails userDetails, String token) {
        Date expirationTime;
        try {
            SignedJWT jwt;
            jwt = SignedJWT.parse(token);
            expirationTime = jwt.getJWTClaimsSet().getExpirationTime();
        } catch (ParseException e) {
            throw new SecurityException(e.getMessage());
        }

        String username = userDetails.getUsername();
        RefreshTokenEntity refreshTokenEntity = createAndStoreRefreshToken(username);
        AuthorizationResponseModel authorizationResponseModel = new AuthorizationResponseModel(expirationTime);
        authorizationResponseModel.setUsername(username);
        authorizationResponseModel.setToken(token);
        authorizationResponseModel.setRefreshToken(refreshTokenEntity.getToken());
        authorizationResponseModel.setRoles(getUserRoles(username));
        authorizationResponseModel.setUserId(userDetails.getId());

        return authorizationResponseModel;
    }


    private RefreshTokenEntity createAndStoreRefreshToken(String username) {
        RefreshTokenEntity refreshTokenEntity = new RefreshTokenEntity();
        refreshTokenEntity.setToken(randomGenerator.generateString(REFRESH_TOKEN_LENGTH));
        UserEntity userEntity = userRepository.findByUsernameAndDeletedFalse(username).orElseThrow(() -> new UserNotFoundException(username));
        refreshTokenEntity.setUser(userEntity);
        refreshTokenRepository.save(refreshTokenEntity);
        return refreshTokenEntity;
    }

    private List<String> getUserRoles(String username) {
        UserEntity userEntity = userRepository.findByUsernameAndDeletedFalse(username).orElseThrow(() -> new UserNotFoundException(username));

        return userEntity.getRoles().stream().map(RoleEntity::getName).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public AuthorizationResponseModel authenticate(String refreshToken) {
        RefreshTokenEntity refreshTokenEntity = refreshTokenRepository.findByToken(refreshToken)
                .orElseThrow(RefreshTokenNotFoundException::new);
        SystemUserDetails userDetails = systemUserDetailsService.loadUserByUsername(refreshTokenEntity.getUser().getUsername());
        String token = jwtTokenProvider
                .createToken(refreshTokenEntity.getUser().getUsername(),
                        userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toSet()));
        refreshTokenRepository.delete(refreshTokenEntity);
        return createAuthorizationResponse(userDetails, token);
    }


    @Override
    @Transactional
    public AuthorizationResponseModel authenticateKey(String key) {
        Optional<UserEntity> userEntity = userRepository.findByKeyAndDeletedFalse(key);
        if (userEntity.isPresent()) {
            SystemUserDetails userDetails = systemUserDetailsService.loadUserByUsername(userEntity.get().getUsername());
            String token = jwtTokenProvider
                    .createToken(userEntity.get().getUsername(),
                            userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                                    .collect(Collectors.toSet()));
            return createAuthorizationResponse(userDetails, token);
        } else {
            String username = randomGenerator.generateString(8);
            String password = randomGenerator.generateString(9);
            register(username, password);
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password, new ArrayList<>()));
            SystemUserDetails userDetails = systemUserDetailsService.loadUserByUsername(username);
            String token = jwtTokenProvider.createToken(username, userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(
                    Collectors.toSet()));
            logger.info("user logged in with username {} and password {}", username, password);
            return createAuthorizationResponse(userDetails, token);
        }

    }

    private void register(String username, String password) {
        UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();
        userRegistrationDTO.setUsername(username);
        userRegistrationDTO.setFirstName("John");
        userRegistrationDTO.setLastName("Smith");
        userRegistrationDTO.setPhoneNumber("077080808");
        userRegistrationDTO.setPassword(password);
        authService.register(userRegistrationDTO);
    }
}
