package com.system.service.user;

import com.system.web.model.auth.AuthorizationResponseModel;

public interface SecurityService {

    AuthorizationResponseModel authenticate(String username, String password);

    AuthorizationResponseModel authenticate(String refreshToken);

    AuthorizationResponseModel authenticateKey(String key);

}
