package com.system.service.user;

import com.system.data.jpa.entity.RoleEntity;
import com.system.data.jpa.entity.UserEntity;
import com.system.data.jpa.repository.RoleRepository;
import com.system.data.jpa.repository.UserRepository;
import com.system.exception.RoleNotFoundException;
import com.system.service.RandomGenerator;
import com.system.service.user.DTO.UserCreateDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final RandomGenerator randomGenerator;

    @Override
    @Transactional
    public void create(UserCreateDTO createDTO) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(createDTO.getUsername());
        userEntity.setEncodedPassword(createDTO.getEncodedPassword());
        userEntity.setFirstName(createDTO.getFirstName());
        userEntity.setLastName(createDTO.getLastName());
        userEntity.setPhoneNumber(createDTO.getPhoneNumber());
        userEntity.setKey(randomGenerator.generateString(10));
        List<RoleEntity> roleEntities = createDTO.getRoles().stream().map(role -> {
            RoleEntity roleEntity = roleRepository.findByName(role.getName())
                    .orElseThrow(() -> new RoleNotFoundException(role.getName()));
            return roleEntity;
        }).collect(Collectors.toList());
        userEntity.setRoles(roleEntities);
        userRepository.save(userEntity);
    }


    public boolean isUserExistWithUsername(String username) {
        Optional<UserEntity> user = userRepository.findByUsernameAndDeletedFalse(username);
        return user.isPresent();
    }

}



