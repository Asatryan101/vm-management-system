package com.system.service.user;

import com.system.service.user.DTO.RoleDTO;


public interface RoleService {

    RoleDTO getByName(String name);

}
