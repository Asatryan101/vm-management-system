package com.system.service.user;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface SystemUserDetailsService extends UserDetailsService {

    SystemUserDetails loadUserByUsername(String var1) throws UsernameNotFoundException;


}
