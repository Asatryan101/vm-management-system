package com.system.service.user;

import com.system.service.user.DTO.UserCreateDTO;

public interface UserService {

    void create(UserCreateDTO user);

    boolean isUserExistWithUsername(String username);


}
