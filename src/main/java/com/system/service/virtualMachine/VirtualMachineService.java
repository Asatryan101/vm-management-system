package com.system.service.virtualMachine;

import com.system.service.virtualMachine.DTO.*;

import java.util.List;

public interface VirtualMachineService {

    VirtualMachineDTO create(VirtualMachineCreateDTO virtualMachineCreateDTO);

    void edit(VirtualMachineEditDTO virtualMachineEditDTO);

    void delete(Long id);

    VirtualMachineDTO getById(Long id);

    List<VirtualMachineDTO> getAll();

    VirtualMachineAssignmentDTO assignUser(VirtualMachineAssignmentCreateDTO virtualMachineAssignmentCreateDTO);

}
