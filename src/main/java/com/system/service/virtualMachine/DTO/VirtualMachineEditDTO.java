package com.system.service.virtualMachine.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualMachineEditDTO extends VirtualMachineCreateDTO {

    private Long id;

}
