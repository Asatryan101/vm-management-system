package com.system.service.virtualMachine.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualMachineAssignmentDTO {

    private VirtualMachineDTO virtualMachine;

    private String username;

}
