package com.system.service.virtualMachine.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualMachineAssignmentCreateDTO {

    private Long id;

    private String username;

}
