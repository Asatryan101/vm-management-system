package com.system.service.virtualMachine.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualMachineDTO {

    private Long id;

    private String name;
}
