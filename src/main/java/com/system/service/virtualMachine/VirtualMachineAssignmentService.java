package com.system.service.virtualMachine;

import com.system.data.jpa.entity.VirtualMachineEntity;
import com.system.data.jpa.repository.VirtualMachineRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class VirtualMachineAssignmentService {

    Logger logger = LoggerFactory.getLogger(VirtualMachineAssignmentService.class);
    private final VirtualMachineRepository virtualMachineRepository;

    public VirtualMachineAssignmentService(VirtualMachineRepository virtualMachineRepository) {
        this.virtualMachineRepository = virtualMachineRepository;
    }

    @Scheduled(fixedRate = 5000)
    private void unAssignVirtualMachine() {
        LocalDateTime currentDate = LocalDateTime.now().minusMinutes(10);
        List<VirtualMachineEntity> virtualMachineEntities = virtualMachineRepository.findAllByCreatedAfterAndDeletedIsFalse(currentDate);
        List<VirtualMachineEntity> collect = virtualMachineEntities.stream().map(v -> {
            v.setUser(null);
            return v;
        }).collect(Collectors.toList());
        logger.info("check virtual machine assignment");
        virtualMachineRepository.saveAll(collect);
    }

}
