package com.system.service.virtualMachine;

import com.system.config.mapping.VirtualMachineMapper;
import com.system.data.jpa.entity.UserEntity;
import com.system.data.jpa.entity.VirtualMachineEntity;
import com.system.data.jpa.repository.UserRepository;
import com.system.data.jpa.repository.VirtualMachineRepository;
import com.system.exception.UserNotFoundException;
import com.system.exception.VirtualMachineAlreadyAssignedException;
import com.system.exception.VirtualMachineNotFoundException;
import com.system.service.user.SecurityServiceImpl;
import com.system.service.virtualMachine.DTO.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
@RequiredArgsConstructor
public class VirtualMachineServiceImpl implements VirtualMachineService {

    private Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);

    private final VirtualMachineRepository virtualMachineRepository;
    private final VirtualMachineMapper virtualMachineMapper;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public VirtualMachineDTO create(VirtualMachineCreateDTO virtualMachineCreateDTO) {
        VirtualMachineEntity virtualMachineEntity = new VirtualMachineEntity();
        virtualMachineEntity.setName(virtualMachineCreateDTO.getName());
        virtualMachineRepository.save(virtualMachineEntity);
        logger.info("virtual machine {} created", virtualMachineEntity.getName());
        return virtualMachineMapper.toDTO(virtualMachineEntity);
    }

    @Override
    @Transactional
    public void edit(VirtualMachineEditDTO virtualMachineEditDTO) {
        VirtualMachineEntity virtualMachineEntity = virtualMachineRepository.findByIdAndDeletedIsFalse(virtualMachineEditDTO.getId())
                .orElseThrow(() -> new VirtualMachineNotFoundException(virtualMachineEditDTO.getId()));
        virtualMachineEntity.setName(virtualMachineEditDTO.getName());
        virtualMachineRepository.save(virtualMachineEntity);
        logger.info("virtual machine {} edited", virtualMachineEntity.getName());
    }

    @Override
    @Transactional
    public void delete(Long id) {
        VirtualMachineEntity virtualMachineEntity = virtualMachineRepository.findByIdAndDeletedIsFalse(id)
                .orElseThrow(() -> new VirtualMachineNotFoundException(id));
        virtualMachineEntity.setDeleted(true);
        virtualMachineRepository.save(virtualMachineEntity);
        logger.info("virtual machine {} deleted", virtualMachineEntity.getName());
    }

    @Override
    @Transactional(readOnly = true)
    public VirtualMachineDTO getById(Long id) {
        VirtualMachineEntity virtualMachineEntity = virtualMachineRepository.findByIdAndDeletedIsFalse(id)
                .orElseThrow(() -> new VirtualMachineNotFoundException(id));
        return virtualMachineMapper.toDTO(virtualMachineEntity);
    }

    @Override
    @Transactional
    public List<VirtualMachineDTO> getAll() {
        List<VirtualMachineEntity> virtualMachineEntities = virtualMachineRepository.findAllByDeletedIsFalse();
        return virtualMachineEntities.stream().map(virtualMachineMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public VirtualMachineAssignmentDTO assignUser(VirtualMachineAssignmentCreateDTO virtualMachineAssignmentCreateDTO) {
        UserEntity userEntity = userRepository.findByUsernameAndDeletedFalse(virtualMachineAssignmentCreateDTO.getUsername())
                .orElseThrow(() -> new UserNotFoundException(virtualMachineAssignmentCreateDTO.getUsername()));
        VirtualMachineEntity virtualMachineEntity = virtualMachineRepository.findByIdAndDeletedIsFalse(virtualMachineAssignmentCreateDTO.getId())
                .orElseThrow(() -> new VirtualMachineNotFoundException(virtualMachineAssignmentCreateDTO.getId()));
        if (nonNull(virtualMachineEntity.getUser())) {
            throw new VirtualMachineAlreadyAssignedException(virtualMachineAssignmentCreateDTO.getId());
        }
        virtualMachineEntity.setUser(userEntity);
        virtualMachineRepository.save(virtualMachineEntity);
        logger.info("virtual machine {} assigned to user {}", virtualMachineEntity.getName(), virtualMachineAssignmentCreateDTO.getUsername());
        return virtualMachineMapper.toAssignmentDTO(virtualMachineEntity);
    }
}
