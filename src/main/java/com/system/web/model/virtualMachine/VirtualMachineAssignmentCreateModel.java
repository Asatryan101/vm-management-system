package com.system.web.model.virtualMachine;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class VirtualMachineAssignmentCreateModel {

    @NotNull
    private Long id;

    @NotNull
    private String username;

}
