package com.system.web.model.virtualMachine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualMachineEditModel extends VirtualMachineCreateModel {

    private Long id;
}
