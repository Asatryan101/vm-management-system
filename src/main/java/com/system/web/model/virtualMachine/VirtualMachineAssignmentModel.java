package com.system.web.model.virtualMachine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualMachineAssignmentModel {

    private VirtualMachineModel virtualMachine;

    private String username;

}
