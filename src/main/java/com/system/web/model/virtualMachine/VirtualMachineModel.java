package com.system.web.model.virtualMachine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualMachineModel {

    private Long id;

    private String name;
}
