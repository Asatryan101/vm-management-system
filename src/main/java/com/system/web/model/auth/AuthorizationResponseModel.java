package com.system.web.model.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.system.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
public class AuthorizationResponseModel {

    @JsonProperty("access_token")
    private String token;

    @JsonProperty("token_type")
    private String tokenType = "Bearer";

    @JsonProperty("refresh_token")
    private String refreshToken;

    @JsonProperty("expires_in")
    private Long expiresIn;

    private String username;

    private Long userId;

    private List<String> roles;

    public AuthorizationResponseModel(Date expiresIn) {
        this.expiresIn = DateUtil.getDateDiff(expiresIn, new Date(), TimeUnit.SECONDS);
    }

}
