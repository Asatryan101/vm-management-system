package com.system.web.model.auth;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefreshTokenModel {

    private String refreshToken;

    private String username;

}
