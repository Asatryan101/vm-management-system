package com.system.web.model.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UserRegisterModel {

    @NotEmpty
    private String phoneNumber;

    @NotEmpty
    private String username;

    @NotEmpty
    @Size(min = 8, max = 15)
    private String password;

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;


}
