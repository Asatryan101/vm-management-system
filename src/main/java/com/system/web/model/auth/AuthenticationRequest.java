package com.system.web.model.auth;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class AuthenticationRequest {

    @NotNull
    private String username;

    @NotNull
    private String password;

}
