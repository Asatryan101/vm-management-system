package com.system.web.rest.controller;

import com.system.config.mapping.UserMapper;
import com.system.exception.RefreshTokenNotFoundException;
import com.system.service.user.AuthService;
import com.system.service.user.DTO.UserRegistrationDTO;
import com.system.service.user.SecurityService;
import com.system.web.model.auth.AuthenticationRequest;
import com.system.web.model.auth.AuthorizationResponseModel;
import com.system.web.model.auth.RefreshTokenModel;
import com.system.web.model.auth.UserRegisterModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final SecurityService securityService;
    private final UserMapper userMapper;
    private final AuthService authService;


    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody @Valid UserRegisterModel userRegisterModel) {
        UserRegistrationDTO userRegistrationDto = userMapper.mapRegisterModelToDto(userRegisterModel);
        authService.register(userRegistrationDto);
        return ResponseEntity.ok(userRegistrationDto);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthorizationResponseModel> login(@RequestBody AuthenticationRequest data) {
        try {
            AuthorizationResponseModel authorizationResponseModel = securityService.authenticate(data.getUsername(), data.getPassword());
            return ResponseEntity.ok(authorizationResponseModel);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    @PostMapping("/login/{key}")
    public ResponseEntity<AuthorizationResponseModel> login(@PathVariable String key) {
        try {
            AuthorizationResponseModel authorizationResponseModel = securityService.authenticateKey(key);
            return ResponseEntity.ok(authorizationResponseModel);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    @PostMapping("/renew")
    public ResponseEntity<AuthorizationResponseModel> renew(@RequestBody RefreshTokenModel data) {
        try {
            AuthorizationResponseModel authorizationResponseModel = securityService.authenticate(data.getRefreshToken());
            return ResponseEntity.ok(authorizationResponseModel);
        } catch (RefreshTokenNotFoundException e) {
            throw new BadCredentialsException("Invalid refreshToken");
        }
    }
}
