package com.system.web.rest.controller;

import com.system.config.mapping.VirtualMachineMapper;
import com.system.service.virtualMachine.DTO.VirtualMachineAssignmentCreateDTO;
import com.system.service.virtualMachine.DTO.VirtualMachineAssignmentDTO;
import com.system.service.virtualMachine.VirtualMachineService;
import com.system.web.model.virtualMachine.VirtualMachineAssignmentCreateModel;
import com.system.web.model.virtualMachine.VirtualMachineAssignmentModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/virtualMachines/assign")
@RequiredArgsConstructor
public class VirtualMachineAssignmentController {

    private final VirtualMachineMapper virtualMachineMapper;
    private final VirtualMachineService virtualMachineService;

    @PostMapping
    public ResponseEntity<VirtualMachineAssignmentModel> create(@RequestBody @Valid VirtualMachineAssignmentCreateModel virtualMachineAssignmentCreateModel) {
        VirtualMachineAssignmentCreateDTO virtualMachineAssignmentCreateDTO = new VirtualMachineAssignmentCreateDTO();
        virtualMachineAssignmentCreateDTO.setId(virtualMachineAssignmentCreateModel.getId());
        virtualMachineAssignmentCreateDTO.setUsername(virtualMachineAssignmentCreateModel.getUsername());
        VirtualMachineAssignmentDTO virtualMachineAssignmentDTO = virtualMachineService.assignUser(virtualMachineAssignmentCreateDTO);
        VirtualMachineAssignmentModel virtualMachineAssignmentModel = virtualMachineMapper.toModel(virtualMachineAssignmentDTO);
        return ResponseEntity.ok(virtualMachineAssignmentModel);
    }
}
