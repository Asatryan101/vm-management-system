package com.system.web.rest.controller;

import com.system.config.mapping.VirtualMachineMapper;
import com.system.service.virtualMachine.DTO.VirtualMachineCreateDTO;
import com.system.service.virtualMachine.DTO.VirtualMachineDTO;
import com.system.service.virtualMachine.DTO.VirtualMachineEditDTO;
import com.system.service.virtualMachine.VirtualMachineService;
import com.system.web.model.virtualMachine.VirtualMachineCreateModel;
import com.system.web.model.virtualMachine.VirtualMachineEditModel;
import com.system.web.model.virtualMachine.VirtualMachineModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/virtualMachines")
@RequiredArgsConstructor
public class VirtualMachineController {

    private final VirtualMachineService virtualMachineService;
    private final VirtualMachineMapper virtualMachineMapper;

    @PostMapping
    public ResponseEntity<VirtualMachineModel> create(@RequestBody @Valid VirtualMachineCreateModel virtualMachineCreateModel) {
        VirtualMachineCreateDTO virtualMachineCreateDTO = new VirtualMachineCreateDTO();
        virtualMachineCreateDTO.setName(virtualMachineCreateModel.getName());
        VirtualMachineDTO virtualMachineDTO = virtualMachineService.create(virtualMachineCreateDTO);
        VirtualMachineModel virtualMachineModel = virtualMachineMapper.toModel(virtualMachineDTO);
        return ResponseEntity.ok(virtualMachineModel);
    }

    @PutMapping("/{id}")
    public void edit(@PathVariable Long id, @RequestBody @Valid VirtualMachineEditModel virtualMachineEditModel) {
        VirtualMachineEditDTO virtualMachineEditDTO = virtualMachineMapper.toDTO(virtualMachineEditModel);
        virtualMachineEditDTO.setId(id);
        virtualMachineEditDTO.setId(id);
        virtualMachineService.edit(virtualMachineEditDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        virtualMachineService.delete(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VirtualMachineModel> get(@PathVariable Long id) {
        VirtualMachineDTO virtualMachineDTO = virtualMachineService.getById(id);
        VirtualMachineModel virtualMachineModel = virtualMachineMapper.toModel(virtualMachineDTO);
        return ResponseEntity.ok(virtualMachineModel);
    }

    @GetMapping
    public ResponseEntity<List<VirtualMachineModel>> getAll() {
        List<VirtualMachineDTO> virtualMachineDTOS = virtualMachineService.getAll();
        List<VirtualMachineModel> virtualMachineModels = virtualMachineDTOS.stream().map(virtualMachineMapper::toModel)
                .collect(Collectors.toList());
        return ResponseEntity.ok(virtualMachineModels);
    }
}
