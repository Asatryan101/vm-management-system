package com.system.data.jpa.repository;

import com.system.data.jpa.entity.VirtualMachineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface VirtualMachineRepository extends JpaRepository<VirtualMachineEntity, Long> {

    Optional<VirtualMachineEntity> findByIdAndDeletedIsFalse(Long id);

    List<VirtualMachineEntity> findAllByDeletedIsFalse();

    List<VirtualMachineEntity> findAllByCreatedAfterAndDeletedIsFalse(LocalDateTime dateBefore);

}
