package com.system.data.jpa.repository;

import com.system.data.jpa.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByUsernameAndDeletedFalse(String username);

    Optional<UserEntity> findByKeyAndDeletedFalse(String username);

}
