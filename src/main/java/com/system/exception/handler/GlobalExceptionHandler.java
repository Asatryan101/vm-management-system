package com.system.exception.handler;


import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

  private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ApiError> defaultErrorHandler(HttpServletRequest req, Exception e) {
    logError(req, e);
    return handleException( e, HttpStatus.INTERNAL_SERVER_ERROR);

  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ResponseEntity<ApiError> httpMessageNotReadableExceptionHandler(HttpServletRequest req, HttpMessageNotReadableException e) {
    logError(req, e);
    ApiError apiError = new ApiError("Malformed JSON request", e);
    return ResponseEntity.badRequest().body(apiError);
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  public ResponseEntity<ApiError> handleDataIntegrityViolationException(HttpServletRequest req, DataIntegrityViolationException e) {
    logError(req, e);
    ApiError apiError;
    if (e.getCause() instanceof ConstraintViolationException && e.getCause().getCause() instanceof SQLIntegrityConstraintViolationException) {
      String errorMessage = e.getCause().getCause().getMessage();
      apiError = new ApiError("Constraint Violations occurred", errorMessage);
    } else {
      apiError = new ApiError("Data Integrity Violations occurred", e);
    }
    return ResponseEntity.badRequest().body(apiError);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ApiError> methodArgumentNotValidExceptionExceptionHandler(HttpServletRequest req, MethodArgumentNotValidException e) {
    logError(req, e);

    List<String> errors = e.getBindingResult()
        .getFieldErrors()
        .stream()
        .map(x -> x.getField() + " " + x.getDefaultMessage())
        .collect(Collectors.toList());

    ApiError apiError = new ApiError("Validation Error", errors.toString());

    return ResponseEntity.badRequest().body(apiError);
  }

  @ExceptionHandler(BadCredentialsException.class)
  public ResponseEntity<ApiError> handleBadCredentialsException(HttpServletRequest req, Exception e) {
    logError(req, e);

    ApiError apiError = new ApiError(e.getMessage(), e);

    return ResponseEntity.badRequest().body(apiError);
  }


  private void logError(HttpServletRequest req, Exception e) {
    logger.warn(e.getMessage());
    logger.warn("RequestURI {}", req.getRequestURI());
    logger.error(ExceptionUtils.getStackTrace(e));
  }

  private ResponseEntity<ApiError> handleException(Exception e, HttpStatus status) {
    ApiError apiError = new ApiError(e);
    return ResponseEntity.status(status).body(apiError);

  }

}
