package com.system.exception.handler;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ApiError {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;

    private String message;

    private String debugMessage;


    public ApiError(Throwable ex) {
        this("Unexpected error", ex);
    }

    public ApiError(String message, Throwable ex) {
        timestamp = LocalDateTime.now();
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
    }

    public ApiError(String message, String debugMessage) {
        timestamp = LocalDateTime.now();
        this.message = message;
        this.debugMessage = debugMessage;
    }

}
