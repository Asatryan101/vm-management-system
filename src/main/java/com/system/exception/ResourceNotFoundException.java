package com.system.exception;

public abstract class ResourceNotFoundException extends RuntimeException {

    protected <T extends Class<?>> ResourceNotFoundException(T type, long id) {

        super(String.format("%s not found: id = %s", type.getSimpleName(), id));
    }

    protected <T extends Class<?>> ResourceNotFoundException(T type, String message) {
        super(String.format("%s not found, %s", type.getSimpleName(), message));
    }
}
