package com.system.exception;


import com.system.service.user.DTO.UserDTO;

public class UserNotFoundException extends ResourceNotFoundException {

    public UserNotFoundException(String username) {
        super(UserDTO.class, username);
    }

}
