package com.system.exception;


import com.system.service.virtualMachine.DTO.VirtualMachineDTO;

public class VirtualMachineNotFoundException extends ResourceNotFoundException {

    public VirtualMachineNotFoundException(Long id) {
        super(VirtualMachineDTO.class, id);
    }

}
