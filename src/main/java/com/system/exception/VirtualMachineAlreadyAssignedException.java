package com.system.exception;


import com.system.service.virtualMachine.DTO.VirtualMachineDTO;

public class VirtualMachineAlreadyAssignedException extends ResourceNotFoundException {

    public VirtualMachineAlreadyAssignedException(Long id) {
        super(VirtualMachineDTO.class, id);
    }

}
