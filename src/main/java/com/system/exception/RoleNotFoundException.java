package com.system.exception;


import com.system.service.user.DTO.RoleDTO;

public class RoleNotFoundException extends ResourceNotFoundException {

    public RoleNotFoundException(String name) {
        super(RoleDTO.class, String.format("Role %s not exist", name));
    }
}
