package com.system.exception;

public class UserAlreadyExistsException extends SecurityException {

  public UserAlreadyExistsException(String username) {
    super(String.format("User already exists username : %s", username));
  }

}
