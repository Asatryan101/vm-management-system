package com.system.util;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class DateUtil {

  private DateUtil() {
    throw new IllegalStateException("DateUtil class");
  }

  /**
   * get difference for date1 and date2.
   * @return difference date1-date2
   */

  public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
    long diffInMillis = date1.getTime() - date2.getTime();
    return timeUnit.convert(diffInMillis, TimeUnit.MILLISECONDS);
  }

  public static Date addDays(Date date, int days) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.DATE, days);
    return cal.getTime();
  }

  public static Date addHours(Date date, int hours) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.HOUR, hours);
    return cal.getTime();
  }

  public static boolean isToday(Date date) {
    return DateUtil.getDateDiff(date, new Date(), TimeUnit.DAYS) == 0;
  }
}
