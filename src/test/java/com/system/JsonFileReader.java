package com.system;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class JsonFileReader {
  private JsonFileReader() {
    throw new IllegalStateException("JsonFileReader");
  }

  public static String readAsText(String path) {

    File file = new File(path);
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.readValue(file, JsonNode.class).toPrettyString();
    } catch (IOException e) {
      return null;
    }
  }
}
