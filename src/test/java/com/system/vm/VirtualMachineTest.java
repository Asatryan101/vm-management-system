package com.system.vm;

import com.system.Application;
import com.system.TestDbCleanerExtension;
import com.system.data.jpa.entity.VirtualMachineEntity;
import com.system.data.jpa.repository.VirtualMachineRepository;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static com.system.TestJsonFileReader.readAsText;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ContextConfiguration(classes = Application.class)
@SpringBootTest
@WebAppConfiguration
@ExtendWith(TestDbCleanerExtension.class)
class VirtualMachineTest {


    private MockMvc mvc;

    @Autowired
    private WebApplicationContext ctx;

    @Autowired
    private VirtualMachineRepository virtualMachineRepository;

    @Autowired
    private UserDetailsService userDetailsService;


    @BeforeEach
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(ctx).apply(springSecurity()).build();
        RestAssuredMockMvc.mockMvc(mvc);
    }

    private Authentication buildMockAuthentication(String username) {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    @Test
    @Transactional
    void create() {
        given().auth().principal(buildMockAuthentication("user"))

                .contentType(ContentType.JSON)
                .body(readAsText("vm/vm-create-valid.json"))

                .when()
                .post("/virtualMachines")

                .then()
                .statusCode(200)

                .assertThat().body("name", equalTo("virtual-machine-4"))
                .assertThat().body("id", equalTo(4));

        VirtualMachineEntity virtualMachineEntity = virtualMachineRepository.findByIdAndDeletedIsFalse(4L).get();
        Assertions.assertThat(virtualMachineEntity.getName()).isEqualTo("virtual-machine-4");
    }

    @Test
    @Transactional
    void createWithInvalidParams() {

        given().auth().principal(buildMockAuthentication("user"))
                .contentType(ContentType.JSON)
                .body(readAsText("vm/vm-create-invalid.json"))

                .when()
                .post("/virtualMachines")

                .then()
                .statusCode(400);
    }

    @Test
    @Transactional
    void edit() {
        given().auth().principal(buildMockAuthentication("user"))

                .contentType(ContentType.JSON)
                .body(readAsText("vm/vm-edit-valid.json"))

                .when()
                .put("/virtualMachines/1")

                .then()
                .statusCode(200);

        VirtualMachineEntity virtualMachineEntity = virtualMachineRepository.findByIdAndDeletedIsFalse(1L).get();
        Assertions.assertThat(virtualMachineEntity.getName()).isEqualTo("virtual-machine-1-edited");
    }


    @Test
    @Transactional
    void editByIdDeletedShouldReturnErrorCode() {

        given().auth().principal(buildMockAuthentication("user"))
                .contentType(ContentType.JSON)
                .body(readAsText("vm/vm-edit-valid.json"))

                .when()
                .put("/virtualMachines/3")

                .then()
                .statusCode(500);
    }

    @Test
    void getById() {

        given().auth().principal(buildMockAuthentication("user"))
                .contentType(ContentType.JSON)

                .when()
                .get("/virtualMachines/2")

                .then()
                .statusCode(200)
                .assertThat().body("id", equalTo(2))
                .assertThat().body("name", equalTo("virtual-machine-2"));
    }

    @Test
    void getByIdDeletedShouldReturnErrorCode() {

        given().auth().principal(buildMockAuthentication("user"))
                .contentType(ContentType.JSON)

                .when()
                .get("/virtualMachines/3")

                .then()
                .statusCode(500);
    }

    @Test
    void getAll() {

        given().auth().principal(buildMockAuthentication("user"))
                .contentType(ContentType.JSON)

                .when()
                .get("/virtualMachines")

                .then()
                .statusCode(200)
                .assertThat().body("id[0]", equalTo(1))
                .assertThat().body("name[0]", equalTo("virtual-machine-1"))
                .assertThat().body("id[1]", equalTo(2))
                .assertThat().body("name[1]", equalTo("virtual-machine-2"));
    }

    @Test
    void deleteById() {
        given().auth().principal(buildMockAuthentication("user"))
                .contentType(ContentType.JSON)
                .when()
                .delete("/virtualMachines/1")
                .then()
                .statusCode(200);
    }
}
