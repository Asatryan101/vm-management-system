package com.system;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class TestDataSource {
  private static Connection connection = null;

  public static Connection getConnection() throws SQLException {
    if (connection == null) {
      connection = createConnection();
    }
    return connection;
  }

  private static Connection createConnection() throws SQLException {
    final HikariConfig config = new HikariConfig();
    HikariDataSource ds;

    System.out.println("connecting to db");
    config.setJdbcUrl("jdbc:mysql://localhost:3306/vmManagement-test?useUnicode=true&connectionCollation=utf8mb4_unicode_ci&characterSetResults=utf8&autoReconnect=true&useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false&allowPublicKeyRetrieval=true");
    config.setUsername("root");
    config.setPassword("password");
    config.addDataSourceProperty("cachePrepStmts", "true");
    config.addDataSourceProperty("prepStmtCacheSize", "250");
    config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
    ds = new HikariDataSource(config);
    return ds.getConnection();
  }

}
