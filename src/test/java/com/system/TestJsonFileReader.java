package com.system;


public class TestJsonFileReader {

  public static String readAsText(String fileName) {
    return JsonFileReader.readAsText("src/test/resources/json-files/" + fileName);
  }
}
