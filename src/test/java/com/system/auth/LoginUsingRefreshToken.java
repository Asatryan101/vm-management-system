package com.system.auth;

import com.system.Application;
import com.system.TestDbCleanerExtension;
import com.system.data.jpa.repository.RefreshTokenRepository;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.module.mockmvc.response.MockMvcResponse;
import io.restassured.response.ExtractableResponse;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.system.TestJsonFileReader.readAsText;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ContextConfiguration(classes = Application.class)
@SpringBootTest
@WebAppConfiguration
@ExtendWith(TestDbCleanerExtension.class)
class LoginUsingRefreshToken {


  private MockMvc mvc;

  @Autowired
  private WebApplicationContext ctx;

  @Autowired
  RefreshTokenRepository refreshTokenRepository;

  @BeforeEach
  public void setUp() {
    this.mvc = MockMvcBuilders.webAppContextSetup(ctx).apply(springSecurity()).build();
    RestAssuredMockMvc.mockMvc(mvc);
  }
  @Test
  void refreshTokenWithValidParamsShouldReturnNewToken() {

        given()
            .contentType(ContentType.JSON)
            .body(readAsText("auth/login/refreshToken.json"))
            .log().all()

            .when()
            .post("auth/renew")

            .then()
            .log().all()
            .statusCode(200)
            .assertThat().body("access_token", notNullValue())
            .assertThat().body("refresh_token", notNullValue())
            .assertThat().body("username", equalTo("user"));

    Assertions.assertThat(refreshTokenRepository.findById(1L)).isNotPresent();
    Assertions.assertThat(refreshTokenRepository.findById(2L)).isPresent();

  }


  @Test
  void refreshTokenWithInvalidParamsShouldReturnForbidden() {

    ExtractableResponse<MockMvcResponse> extract;

    extract = given()
        .contentType(ContentType.JSON)
        .body(readAsText("auth/login/invalid-refreshToken.json"))
        .log().all()

        .when()
        .post("auth/renew")

        .then()
        .log().all()
        .statusCode(400)
        .assertThat().body("message", equalTo("Invalid refreshToken"))
        .extract();
  }
}
