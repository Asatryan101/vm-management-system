package com.system.auth;

import com.system.Application;
import com.system.TestDbCleanerExtension;
import com.system.data.jpa.entity.UserEntity;
import com.system.data.jpa.repository.UserRepository;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ContextConfiguration(classes = Application.class)
@SpringBootTest
@WebAppConfiguration
@ExtendWith(TestDbCleanerExtension.class)
class LoginUsingKey {


    private MockMvc mvc;

    @Autowired
    private WebApplicationContext ctx;

    @Autowired
    UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(ctx).apply(springSecurity()).build();
        RestAssuredMockMvc.mockMvc(mvc);
    }

    @Test
    void loginUsingUserKey() {

        given()
                .contentType(ContentType.JSON)

                .when()
                .post("auth/login/key-1")

                .then()
                .log().all()
                .statusCode(200)
                .assertThat().body("access_token", notNullValue())
                .assertThat().body("refresh_token", notNullValue())
                .assertThat().body("username", equalTo("user"));

        UserEntity userEntity = userRepository.findByKeyAndDeletedFalse("key-1").get();
        Assertions.assertThat(userEntity.getUsername()).isEqualTo("user");
    }
}