package com.system;

import org.hibernate.tool.hbm2ddl.MultipleLinesSqlCommandExtractor;
import org.hibernate.tool.schema.internal.exec.ScriptSourceInputFromFile;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TestDbCleanerExtension implements BeforeEachCallback {
  private static final Logger logger = LoggerFactory.getLogger(TestDbCleanerExtension.class);

  private List<String> readSQLImportScript() {
    File file = new File("src/test/resources/db/data.sql");
    MultipleLinesSqlCommandExtractor commandExtractor = new MultipleLinesSqlCommandExtractor();

    ScriptSourceInputFromFile importScriptInput = new ScriptSourceInputFromFile(file, "UTF-8");

    List<String> commands = new ArrayList<>();
    importScriptInput.prepare();
    try {
      commands.addAll(importScriptInput.read(commandExtractor));
    } finally {
      importScriptInput.release();
    }
    return commands;

  }

  @Override
  public void beforeEach(ExtensionContext context) throws Exception {
    Connection con = TestDataSource.getConnection();
    Statement st = con.createStatement();
    con.setAutoCommit(false);
    for (String command : readSQLImportScript()) {
      logger.debug(command);
      st.execute(command);
    }
    con.commit();
  }
}




