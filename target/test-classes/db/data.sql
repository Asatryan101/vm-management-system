SET
FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE user;
TRUNCATE TABLE role;
TRUNCATE TABLE user_role;
TRUNCATE TABLE refresh_token;
TRUNCATE TABLE virtual_machine;



SET
FOREIGN_KEY_CHECKS = 1;

--USER
INSERT INTO `user` (id, `encoded_password`, `username`, `phone_number`, `first_name`, `last_name`, `user_key`,
                    `deleted`)
VALUES (1, '$2a$10$2vR0qCxUzFGdwo.zIELvVuJEO6/fcEArSBoTak7gmo8VXcSio9L8y', 'user', '123456789', 'firstName-1',
        'lastName-1', 'key-1', false);

INSERT INTO `user` (id, `encoded_password`, `username`, `phone_number`, `first_name`, `last_name`, `user_key`,
                    `deleted`)
VALUES (2, '$2a$10$2vR0qCxUzFGdwo.zIELvVuJEO6/fcEArSBoTak7gmo8VXcSio9L8y', 'admin', '123456781', 'firstName-2',
        'lastName-2', 'key-2', false);

--ROLE
INSERT INTO `role` (id, `name`)
VALUES (1, 'ROLE_USER');

INSERT INTO `role` (id, `name`)
VALUES (2, 'ROLE_ADMIN');

--USER_ROLE
insert into user_role (user_id, role_id)
values (1, 1);

insert into user_role (user_id, role_id)
values (2, 1);

insert into user_role (user_id, role_id)
values (2, 2);

--REFRESH_TOKEN
INSERT INTO `refresh_token` (id, `token`, `user_id`)
VALUES (1,
        'jD6EsIcA7v3onvvvN9xq81GeGc7nhpWMpnaJ4t0Zb3zKx9cCy90KSnJWkvAp4jAhlKqRW7EMyLd6uX9fxpQ1g7IIZxHwzuXTIthkop3tdnjvvjeMrOWTQBQpRtWvJDVDWW4ezI661znvXZrXr0YNzmMiKPs6LOEgBVLRIc8ZlQ91Jljm594994KJE6EW1arkzkyZmHVVkAmre6onpPPjtq9TbmyMZglM6HJS3cr5xRF0mpGjxlGHWkvLGKsBI9A',
        1);


-- VIRTUAL_MACHINES
INSERT INTO `virtual_machine`(id, `name`, `deleted`, `user_id`, `created`)
VALUES (1, 'virtual-machine-1', false, 1, '2021-06-18 18:17:38');

INSERT INTO `virtual_machine`(id, `name`, `deleted`, `user_id`, `created`)
VALUES (2, 'virtual-machine-2', false, 2, '2021-06-18 18:17:38');

INSERT INTO `virtual_machine`(id, `name`, `deleted`, `user_id`, `created`)
VALUES (3, 'virtual-machine-3', true , 1, '2021-06-18 18:17:38');