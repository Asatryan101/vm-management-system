package com.system.config.mapping;

import com.system.data.jpa.entity.VirtualMachineEntity;
import com.system.service.virtualMachine.DTO.VirtualMachineAssignmentDTO;
import com.system.service.virtualMachine.DTO.VirtualMachineDTO;
import com.system.service.virtualMachine.DTO.VirtualMachineEditDTO;
import com.system.web.model.virtualMachine.VirtualMachineAssignmentModel;
import com.system.web.model.virtualMachine.VirtualMachineEditModel;
import com.system.web.model.virtualMachine.VirtualMachineModel;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-06-11T20:32:18+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.10 (Amazon.com Inc.)"
)
public class VirtualMachineMapperImpl implements VirtualMachineMapper {

    @Override
    public VirtualMachineDTO toDTO(VirtualMachineEntity virtualMachineEntity) {
        if ( virtualMachineEntity == null ) {
            return null;
        }

        VirtualMachineDTO virtualMachineDTO = new VirtualMachineDTO();

        virtualMachineDTO.setId( virtualMachineEntity.getId() );
        virtualMachineDTO.setName( virtualMachineEntity.getName() );

        return virtualMachineDTO;
    }

    @Override
    public VirtualMachineAssignmentDTO toAssignmentDTO(VirtualMachineEntity virtualMachineEntity) {
        if ( virtualMachineEntity == null ) {
            return null;
        }

        VirtualMachineAssignmentDTO virtualMachineAssignmentDTO = new VirtualMachineAssignmentDTO();

        return virtualMachineAssignmentDTO;
    }

    @Override
    public VirtualMachineEditDTO toDTO(VirtualMachineEditModel vmEditModel) {
        if ( vmEditModel == null ) {
            return null;
        }

        VirtualMachineEditDTO virtualMachineEditDTO = new VirtualMachineEditDTO();

        virtualMachineEditDTO.setName( vmEditModel.getName() );
        virtualMachineEditDTO.setId( vmEditModel.getId() );

        return virtualMachineEditDTO;
    }

    @Override
    public VirtualMachineModel toModel(VirtualMachineDTO virtualMachineDTO) {
        if ( virtualMachineDTO == null ) {
            return null;
        }

        VirtualMachineModel virtualMachineModel = new VirtualMachineModel();

        virtualMachineModel.setId( virtualMachineDTO.getId() );
        virtualMachineModel.setName( virtualMachineDTO.getName() );

        return virtualMachineModel;
    }

    @Override
    public VirtualMachineAssignmentModel toModel(VirtualMachineAssignmentDTO virtualMachineAssignmentDTO) {
        if ( virtualMachineAssignmentDTO == null ) {
            return null;
        }

        VirtualMachineAssignmentModel virtualMachineAssignmentModel = new VirtualMachineAssignmentModel();

        virtualMachineAssignmentModel.setVirtualMachine( toModel( virtualMachineAssignmentDTO.getVirtualMachine() ) );
        virtualMachineAssignmentModel.setUsername( virtualMachineAssignmentDTO.getUsername() );

        return virtualMachineAssignmentModel;
    }
}
