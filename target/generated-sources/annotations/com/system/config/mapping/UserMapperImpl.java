package com.system.config.mapping;

import com.system.service.user.DTO.UserCreateDTO;
import com.system.service.user.DTO.UserRegistrationDTO;
import com.system.web.model.auth.UserRegisterModel;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-06-11T20:32:18+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.10 (Amazon.com Inc.)"
)
public class UserMapperImpl implements UserMapper {

    @Override
    public UserCreateDTO mapRegistrationDTOToCreateDTO(UserRegistrationDTO registrationDto) {
        if ( registrationDto == null ) {
            return null;
        }

        UserCreateDTO userCreateDTO = new UserCreateDTO();

        userCreateDTO.setUsername( registrationDto.getUsername() );
        userCreateDTO.setPhoneNumber( registrationDto.getPhoneNumber() );
        userCreateDTO.setFirstName( registrationDto.getFirstName() );
        userCreateDTO.setLastName( registrationDto.getLastName() );

        return userCreateDTO;
    }

    @Override
    public UserRegistrationDTO mapRegisterModelToDto(UserRegisterModel userRegisterModel) {
        if ( userRegisterModel == null ) {
            return null;
        }

        UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();

        userRegistrationDTO.setPhoneNumber( userRegisterModel.getPhoneNumber() );
        userRegistrationDTO.setUsername( userRegisterModel.getUsername() );
        userRegistrationDTO.setPassword( userRegisterModel.getPassword() );
        userRegistrationDTO.setFirstName( userRegisterModel.getFirstName() );
        userRegistrationDTO.setLastName( userRegisterModel.getLastName() );

        return userRegistrationDTO;
    }
}
